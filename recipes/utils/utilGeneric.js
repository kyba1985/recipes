"use_strict";
const replaceAll = require("replaceall");

/*
un'
d'
l'
'
:
(
)
...
....
,
.
all'
;
*/

function removeBadCharsFromIngredients(word) {
    var word = replaceAll("un'", "", word);
    word = replaceAll("d'", "", word);
    word = replaceAll("l'", "", word);
    word = replaceAll("'", "", word);
    word = replaceAll(":", "", word);
    word = replaceAll("(", "", word);
    word = replaceAll(")", "", word);
    word = replaceAll("...", "", word);
    word = replaceAll("....", "", word);
    word = replaceAll(",", "", word);
    word = replaceAll(".", "", word);
    word = replaceAll("all'", "", word);
    word = replaceAll(";", "", word);
    word = replaceAll("\ ", "", word);
    return word;
}

function getPluralAndSingolar(word) {
    var words = [];
    words.push(word);
    if (word.toLowerCase().endsWith("cca")) {
        words.push(word.toLowerCase().replace("cca", "cche"));
    }
    else if (word.toLowerCase().endsWith("cche")) {
        words.push(word.toLowerCase().replace("cche", "cca"));
    }
    else if (word.toLowerCase().endsWith("cco")) {
        words.push(word.toLowerCase().replace("cchi", "cco"));
    }
    else if (word.toLowerCase().endsWith("cchi")) {
        words.push(word.toLowerCase().replace("cchi", "cco"));
    }
    else if (word.toLowerCase().endsWith("a")) {
        words.push(word.replace(/a$/, "e"));
    }
    else if (word.toLowerCase().endsWith("e")) {
        words.push(word.replace(/e$/, "a"));
    }
    else if (word.toLowerCase().endsWith("i")) {
        words.push(word.replace(/i$/, "o"));
    }
    else if (word.toLowerCase().endsWith("o")) {
        words.push(word.replace(/o$/, "i"));
    }

    return words;
}

exports.getPluralAndSingolar = getPluralAndSingolar;
exports.removeBadCharsFromIngredients = removeBadCharsFromIngredients;