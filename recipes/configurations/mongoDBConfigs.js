"use_strict";

const DBConnectionString = "mongodb://READWRITECLIENTSERVER:READWRITECLIENTSERVER@recipes-shard-00-00-dspdp.gcp.mongodb.net:27017,recipes-shard-00-01-dspdp.gcp.mongodb.net:27017,recipes-shard-00-02-dspdp.gcp.mongodb.net:27017/test?ssl=true&replicaSet=recipes-shard-0&authSource=admin&retryWrites=true";

const DBConfig = {
    dbConnectionString: DBConnectionString
}

module.exports = DBConfig;