"use_strict";

var express = require('express');
var router = express.Router();
var recipeController = require("../controllers/recipescontroller");

/* GET recipes listing. 
query params:
difficult , ingredients , totaltime , totaltimeFilter , preparationtime, preparationtimeFilter , cookingtime , cookingtimeFilter , type , pagesize , paginationindex , namerecipe
fields
*/

router.get('/:idrecipe', function (req, res, next) {
    console.log("query: " + JSON.stringify(req.params));
    var idToSearch = req.params.idrecipe;
    recipeController.findRecipe(idToSearch).then(function (data) {
        res.status(200).json(data);
    }).catch(function (err) {
        res.status(500).json({ "error": err });
    });

});

router.get('/', function (req, res, next) {
    console.log("query: " + JSON.stringify(req.query));
    var query = {};
    query.difficult = req.query.difficult;
    if (req.query.ingredients) {
        query.ingredients = [];
        query.ingredients = query.ingredients.concat(req.query.ingredients);
    }
    query.totaltime = req.query.totaltime;
    query.totaltimefilter = req.query.totaltimefilter;
    query.preparationtime = req.query.preparationtime;
    query.preparationtimefilter = req.query.preparationtimefilter;
    query.cookingtime = req.query.cookingtime;
    query.cookingtimefilter = req.query.cookingtimefilter;
    query.pagesize = req.query.pagesize;
    query.paginationindex = req.query.paginationindex;
    query.fields = req.query.fields;
    query.indredients = req.query.indredients;
    query.namerecipe = req.query.namerecipe;
    query.type = req.query.type;
    query.fieds = req.query.fieds;
    if (req.query.without) {
        query.without = [];
        query.without = query.without.concat(req.query.without);
    }
    query.sortby = req.query.sortby;

    console.log("query: " + JSON.stringify(query));
    recipeController.getAllRecipes(query).then(function (data) {
        res.status(200).json(data);
    }).catch(function (err) {
        res.status(500).json({ "error": err });
    }); //todo capire come gestire gli errori in maniera centralizzata , qui c'era un next che pero faceva crashare
});


/* ADD a new recipe or update if needs*/
router.post('/', function (req, res, next) {
    //sample add     
    var dataNewRecipe = req.body;
    recipeController.addNewRecipe(dataNewRecipe).then(function (data) {
        res.status(200).json({ "operation add recipe": "ok" });
    }).catch(next);
});

/* ADD a new recipe from external source*/
router.post('/import', function (req, res, next) {
    //sample add     
    console.log("data:" + JSON.stringify(req.body));
    //    var dataNewRecipe = req.body;
    //    recipeController.addNewRecipe(dataNewRecipe).then(function (data) {
    //        res.status(200).json({ "operation add recipe": "ok" });
    //    }).catch(next);
    res.status(200).json({ "import completed with status": "ok" });
});



module.exports = router;