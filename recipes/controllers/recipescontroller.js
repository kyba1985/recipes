"use_strict";

const RepositoryRecipes = require("../repository/recipeRepository");

async function getAllRecipes(query) {
    var recipes = await RepositoryRecipes.loadRecipes(query).catch(error => {
        console.log(error);
        return Promise.reject(new Error("Database Error: " + error)); //capire meglio come gestire questi errori
    });
    return recipes;
}

async function addNewRecipe(recipe) { //rivedere questa struttura non viene bene col catch
    try {
        var recipeId = await RepositoryRecipes.addNewRecipe(recipe);
        return recipeId;
    } catch (err) {
        throw new Error("Database Error: " + err);
    }
}

async function findRecipe(idRecipe) {
    try {
        var recipe = await RepositoryRecipes.findRecipe(idRecipe);
        return recipe;
    } catch (err) {
        throw new Error("Database Error: " + err);
    }
}



exports.addNewRecipe = addNewRecipe;
exports.getAllRecipes = getAllRecipes;
exports.findRecipe = findRecipe;

