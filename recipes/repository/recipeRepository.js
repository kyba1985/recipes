"use_strict";

//write a recipe on file with base folder path
const path = require("path");
const fs = require("fs-extra");
const DBConfig = require("../configurations/mongoDBConfigs");
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;
const MAXSIZEPAGE = 2;
const MAXSIZEPAGEFINDFORINDREDIENTS = 8;

function findRecipe(idRecipe) {
    return new Promise((resolve, reject) => {
        var o_id_Recipe = new ObjectId(idRecipe);
        var queryDb = { _id: o_id_Recipe };
        var url = DBConfig.dbConnectionString;
        MongoClient.connect(url, function (err, client) {
            console.log('connected error? : ' + err);
            if (client) {
                client.db("recipesdb").collection("recipes").findOne(queryDb, (err, recipe) => {
                    client.close();
                    if (err)
                        reject(err);
                    else
                        resolve(recipe);
                });
            }
            else {
                reject(new Error("Connection error to DB: " + err));
            }
        });

    });
}

function loadRecipes(query) {
    return new Promise((resolve, reject) => {

        //limit
        let maxPageSizeForQuery = (query.ingredients || query.without) ? MAXSIZEPAGEFINDFORINDREDIENTS : MAXSIZEPAGE;
        var limit = query.pagesize ? +query.pagesize : maxPageSizeForQuery;

        //skip pagination index
        var skip = Number(query.paginationindex);
        skip = skip > 0 ? skip : 0;

        var queryDb = {};
        var sort = {};
        //difficult
        if (query.difficult) queryDb.difficult = query.difficult;

        //times
        if (query.totaltime && query.totaltimefilter) {
            if (query.totaltimefilter === "above") queryDb.totalTime = { $gte: Number(query.totaltime) };
            else if (query.totaltimefilter === "below") queryDb.totalTime = { $lte: Number(query.totaltime) };
            else queryDb.totalTime = { $eq: Number(query.totaltime) };
        }

        if (query.cookingtime && query.cookingtimefilter) {
            if (query.cookingtimefilter === "above") queryDb.cookeTime = { $gte: Number(query.cookingtime) };
            else if (query.cookingtimefilter === "below") queryDb.cookeTime = { $lte: Number(query.cookingtime) };
            else queryDb.cookeTime = { $eq: Number(query.cookingtime) };
        }

        if (query.preparationtime && query.preparationtimefilter) {
            if (query.preparationtimefilter === "above") queryDb.preparationTime = { $gte: Number(query.preparationtime) };
            else if (query.preparationtimefilter === "below") queryDb.preparationTime = { $lte: Number(query.preparationtime) };
            else queryDb.preparationTime = { $eq: Number(query.preparationtime) };
        }

        //ingredients
        if (query.ingredients) {
            queryDb.ingredientiPuri = { $all: query.ingredients }
        };

        //name
        if (query.namerecipe) {
            queryDb.title = { $regex: query.namerecipe };
        }

        //type
        if (query.type) queryDb.type = query.type;

        //sort
        if (query.sortby) {
            var sortOn = query.sortby;
            if (sortOn === "update") {
                sort = { updateTimeStampEpoch: -1 };
            }
            else if (sortOn === "create") {
                sort = { creationDataOnDb: -1 }
            }
            else {
                sort = { creationDataOnDb: -1 }
            }
        }
        else {
            sort = { creationDataOnDb: -1 }
        }

        //without , need create and and condition
        if (query.without) //array of elements
        {
            /* sample query and 
            {$and:[{ $or : [ { title : {$regex : "pasta"} }, { title : { $exists : true} } ] },{ $and : [ { ingredientiPuri : { $all : ["salsiccia","funghi"]} }, { ingredientiPuri : { $nin : ["cipolla"] } } ] }]}
            */
            //2 cases , with and without or only without
            if (query.ingredients) {
                //per ogni elemento lo devo mettere in or per poi mettere in and la parte degli ingredienti
                var queryWithAnds = { $and: [] };
                if (query.type) {
                    var orType = { $or: [{ "type": query.type }, { "type": { $exists: true } }] };
                    queryWithAnds.$and.push(orType);
                }
                if (query.namerecipe) {
                    var orNameRecipe = { $or: [{ "title": { $regex: query.namerecipe } }, { "title": { $exists: true } }] };
                    queryWithAnds.$and.push(orNameRecipe);
                }
                if (query.totaltime && query.totaltimefilter) {
                    var orTotalTime;
                    if (query.totaltimefilter === "above") orTotalTime = { $or: [{ "totaltime": { $gte: Number(query.totaltime) } }, { "totaltime": { $exists: true } }] };
                    else if (query.totaltimefilter === "below") orTotalTime = { $or: [{ "totaltime": { $lte: Number(query.totaltime) } }, { "totaltime": { $exists: true } }] };
                    else orTotalTime = { $or: [{ "totaltime": { $eq: Number(query.totaltime) } }, { "totaltime": { $exists: true } }] };
                    queryWithAnds.$and.push(orTotalTime);
                }
                //todo altri parametri da mettere in or se previsti

                //ultima parte piu importante and di all e nin
                var andIngredienti = { $and: [{ ingredientiPuri: { $all: query.ingredients } }, { ingredientiPuri: { $nin: query.without } }] };
                queryWithAnds.$and.push(andIngredienti);

                queryDb = queryWithAnds;
            }
            else //only without
            {
                queryDb.ingredientiPuri = { $nin: query.ingredients };
            }
        }

        var projectionFields = {};
        if (query.fields) {
            projectionFields = createProjectionFields(query.fields);
        }

        console.log("query: " + JSON.stringify(queryDb));
        console.log("projectionFields: " + JSON.stringify(projectionFields));

        var url = DBConfig.dbConnectionString;
        MongoClient.connect(url, function (err, client) {
            console.log('connected error? : ' + err);
            if (client) {
                client.db("recipesdb").collection("recipes").find(queryDb, projectionFields).limit(limit).skip(skip).sort(sort).toArray(function (err, items) {
                    if (err)
                        reject(err);
                    else
                        resolve(items);
                    client.close();
                });
            }
            else {
                reject(new Error("Connection error to DB: " + err));
            }
        });
    });
}

function createProjectionFields(fields) //fields is a string like (field1,field2,field3)
{
    try {
        //TODO from fields in query to object
        fields = fields.replace("(", "");
        fields = fields.replace(")", "");
        var fieldsNames = fields.split(",");
        var objectFieldsProjection = {};
        var projection = {};
        projection["_id"] = 1;
        fieldsNames.forEach(field => {
            projection["" + field] = 1;
        });
        objectFieldsProjection["projection"] = projection;
        return objectFieldsProjection;
    } catch (err) {
        return {};
    }
}

function addNewRecipe(recipe, override = false) {
    return new Promise((resolve, reject) => {
        var url = DBConfig.dbConnectionString;
        MongoClient.connect(url, function (err, client) {
            console.log('connected? : ' + err);
            //check if recipe exists and check if update the recipe
            var queryRecipe = {};
            queryRecipe.title = { $regex: recipe.title };
            queryRecipe.source = recipe.source;
            client.db("recipesdb").collection("recipes").findOne(queryRecipe, function (err, recipeOld) {
                if (err)
                    reject(err);
                else {
                    //check if updated and if recipeold exists
                    if (override || !recipeOld || (recipeOld && recipe.updateTimeStampEpoch > recipeOld.updateTimeStampEpoch) || recipe.hash !== recipeOld.hash) {
                        recipe.creationDataOnDb = new Date().getTime();
                        client.db("recipesdb").collection("recipes").insertOne(recipe).then(function (data) {
                            resolve(data.insertedId);
                            client.close();
                        }).catch(function (err) {
                            client.close();
                            reject(err);
                        });
                    }
                    else {
                        console.log("recipe present in db is updated");
                        resolve(recipeOld.insertedId);
                    }
                }
            });
        });
    });
}

const saveRecipeOnFile = async (folderPath, recipe) => {
    try {
        result = await writeFileAsynk(folderPath, recipe);
        return result;
    } catch (err) {
        console.log(err);
        return false;
    }
}

function writeFileAsynk(folderPath, recipe) {
    var pathFile = path.join(folderPath, recipe.title + ".txt");
    return new Promise((resolve, reject) => {
        fs.ensureDir(folderPath).then(() => {
            fs.writeFile(pathFile, JSON.stringify(recipe), (err) => {
                if (err)
                    resolve(false);
                else
                    resolve(true);
            });
        });
    });
}

exports.addNewRecipe = addNewRecipe;
exports.loadRecipes = loadRecipes;
exports.saveRecipeOnFile = saveRecipeOnFile;
exports.findRecipe = findRecipe;