"use_strict";


const fs = require("fs-extra");
var path = require('path');
const linkExtractor = require("../scripts/link_extractor_aia");
const AiaRecipeParser = require("../scripts/recipe_aia_parser");
const RecipeRepository = require("../repository/recipeRepository");
const sourceAia = "aia";
const Queue = require("promise-queue");
const baseUrlRecipeAia = "http://www.aiafood.com/it/ricette";
const urlLogoAia = "http://www.aiafood.com/sites/all/themes/aiafood/images/le-idee-di-aia.png"; //TODO sostituire con una immagine nel cloud di google

var maxConcurrent = 5;
var maxQueue = Infinity;
var queue = new Queue(maxConcurrent, maxQueue);

var badwordsAIA = ["aequilibrium", "caldo", "di", "a", "freddo", "aia", "un", "mezzo", "bustina", "confezione", "forno", "giallo", "taggiasche", "rossi", "rossa", "verde", "costa", "tazzina", "bianco", "cucchiai", "secca", "(noci,", "mandorle,", "nocciole,", "pinoli)", "vasetto", "greco", "bustine", "fiocchi", "aneto", "suino", "rotoli", "pasta", "fresche", "grandissime", "oro", "natura", "rettangolare", "reggiano", "grattugiato", "canna", "allevate", "all’aperto", "mascarpone", "semolato", "amaro", "polvere", "q.b.", "velo", "fresco", "ali", "maxi", "cucchiaini", "dolce", "grattato", "friggere", "pizzy", "snack", "party", "nagghy", "gambi", "classica", "stuzzichini", "carnealfuoco", "rosso", "ramati", "capperini", "salati", "d’aceto", "semi", "d’oliva", "intero", "fresca", "rametti", "foglia", "confezioni", "(basilico,", "cipollina,", "timo,", "maggiorana)", "extra", "vergine", "cespi", "belga", "teste", "tardivo", "integrale", "tipo", "cucina", "classico", "secco", "(facoltativo)", "gran", "prugne,", "spicchio", "d’aglio", "mazzetto", "d'aglio", "q.b", "disossato", "mazzo", "rametto", "1/2", "piccola", "mezza", "spicchi", "una", "scottatine", "–", "sottilissime", "uno", "purè:", "grattugiata", "tazza", "vaschetta", "freschi", "delizie", "dell’orto", "rotolo", "etti", "o", "scaglie", "alla", "grosse", "foglioline", "(o", "limone)", "qualche", "spalmabile", "disidratato", "rosse", "grandi", "chiodi", "¾", "bottiglia", "lattina", "ciuffo", "vegetale", "paesana", "bocconcini", "fettine", "¼", "bianca", "media", "topinambur", "mature", "fogliolina", "bronte", "taggia", "fermo", "cumino", "seme", "pezzetto", "naturale", "non", "bicchierino", "d'olio", "sgusciati", "bastoncini", "bianche", "d'uovo", "pizzaiola", "lunghe", "aromatiche", "(rosmarino,", "salvia,", "maggiorana,", "timo)", "già", "cotta", "maturo", "d’acqua", "sott’olio", "capperi", "filetti", "acciuga", "calvados", "acidule", "secchi", "sgusciate", "misticanza", "foglie", "cervere", "granella", "pelate", "½", "cuori", "gialli", "pistacchi,", "zucca)", "padano", "vaccina", "fili", "arancione", "verdi", "piccole", "d'oliva", "pronta", "q.t.", "borettane", "casereccio", "piccante", "cime", "fiori", "eduli", "(bicolore", "nera)", "alle", "cotti", "venere", "grani", "rustico", "3/4", "gialli,", "arancioni", "insalato", "puro", "la", "milanese", "cespo", "magro", "zuccherato", "romana", "worcestershire", "sauce", "etto", "raffermo", "macinata", "all’uovo", "gambo", "scremato", "denocciolate", "piccoli", "sotto", "scarso", "busta", "imperiale", "busto", "verzino", "polpa", "all'uovo", "(tipo", "fresche)", "quarti", "romano", "colorante", "alimentare", "(in", "polvere)", "semolato", "romani", "(mammole)", "spremuto", "macinato", "bovino", "tritato", "finissimo", "mollica", "alfa", "piccanti", "(gialli", "rossi)", "frizzante", "gelata", "punta", "coltello", "piedini", "musetto", "terra", "oppure", "vasetti", "caprino", "buccia", "disidratata", "decorare", "piacere", "intere", "valdostana", "barbecue", "salad", "taccole", "radice", "trattato", "sceltissima", "barattolo", "precotti", "macinati", "chips", "dolci", "buona", "scoperta", "tramezzini", "stagione", "cuore", "bue", "carote", "bruxelles", "grande", "d'alloro", "temperatura", "ambiente", "tagliato", "cubetti", "champignon)", "parti", "gaeta", "nere", "d’alloro", "rosmarino,", "pezzi", "fredda", "agrodolce:", "pantelleria", "galline", "cous", "1k", "ingredienti", "olive:", "piccolo", "cubetto", "1cucchiaio", "d’acacia", "cordon", "bleu", "raso", "rosa", "fusi", "ribes", "affumicata", "tagliata", "metà", "d’arancia", "(1", "bicchiere)", "happy", "-", "marinatura", "fettina", "millefiori", "chiara", "viennese", "messicana:", "grandezza", "essiccato", "panate", "pronto", "cipolla,", "carota,", "sedano,", "alloro)", "sminuzzato", "grattuggiato", "laviennese", "3-4", "10-12", "medie", "dimensioni", "sorrento", "filo", "cotolette", "sakura", "gherigli", "pastagialla", "d’uovo", "scorza", "maizena", "pack", "ghiaccio", "panettone", "vanigliato", "litro", "all'aperto", "parzialmente", "gialle", "1,5", "cassetta", "caprini", "bio", "vita", "manitoba", "frolla", "biologica", "(freddo", "frigorifero)", "gialla", "allevato", "estratto", "cacao:", "pasticcera:", "guarnizione:", "toscano", "ciuffetto", "allungate", "gruyère", "(solo", "tuorlo)", "scatola", "concentrato", "pronti", "tagliate", "finemente", "secche", "balsamico", "4-8", "aia", "farina", "dolci", "velo", "lesse", "fillo", "fogli", "(timo,", "rosmarino)", "classiche", "bionde", "gelatina", "d’alici", "manciata", "brandy", "brisée", "integrali", "(100", "totali)", "cottomagro", "freschissimi", "focaccia:", "tiepida", "farcitura:", "delicato", "100%", "salata", "lessati", "panetto", "pizza", "maldon", "lungo", "(scamorza", "affumicata,", "fontina,…)", "cornetti", "frittatina", "acqua)", "zero", "all’anice", "fino", "grosso", "tostato", "tostate", "0,3", "padovanella", "grandissimo", "lt", "vin", "essiccati", "trancetto", "novello", "(mandorle,", "noci,", "dura", "coscia", "forte", "gramigna", "senza", "pelle", "150/200", "cucchiaiate", "worchestershire", "panino", "alternativa", "lattuga)", "tondi", "glassa", "goccia", "cheddar", "(meglio", "se", "sesamo)", "ketchup,", "maionese,", "laugenbrot", "grattugiare", "bavaria", "lunghi", "morbidi", "hot", "inglese", "colman’s)", "bionda", "ai", "farcire", "filone", "duro", "formato", "quadrato)", "renetta", "mista", "(radicchio,", "2/3", "(gialli,", "verdi)", "fini", "piante", "corta", "trevigiana", "cicorino", "alternativa)", "parboiled", "petti", "trito", "scelta", "lollo", "melagrana", "bratwurst", "ratte", "(patate", "francesi)", "4-5", "antica", "chicchi", "lavato", "emulsione", "arancio", "(semi", "zucca,", "lino,", "gomasio", "sodi", "maturi", "tubetto", "silano", "arrotolata", "stuzzicadenti", "allo", "yogurt:", "sottili", "carta", "involtini", "(zucca,", "finocchio,", "girasole,", "sesamo,", "papavero)", "(crudo", "scelta)", "precotte", "mix", "cinesi", "keb’s", "keb's", "(surgelato)", "5-6", "colorati", "passata)", "baby", "bollite", "d'arancia", "stagionata", "codette", "colorate", "scura", "misto", "cosa", "pomodoro)", "rettangoli", "camicia", "pesto:", "marino", "parigiano", "paio", "ramato", "selvatico", "un'arancia", "si", "frigorifero", "aceto)", "bigger", "scuro", "circa", "aia)", "blocchi", "cremor", "tartaro", "all’origano", "carasau", "grigliate", "mini", "due", "brisèe", "surgelati", "fleur", "de", "tonde", "torte", "salate", "istantanea", "goccio", "(pack", "4)", "favette", "caramello", "salato", "secche)", "bibita", "gassata", "arso", "perlato", "ossibuchi", "tacchino,", "(bomba)", "surgelato", "affettato", "tostati", "fonduta:", "grammi", "istantaneo", "tartara", "patè", "affumicato", "arabi", "frittura", "francesino", "(bianchi,", "cereali,", "integrali,..)", "san", "marzano", "fior", "rigate", "pulite", "(cannellini", "borlotti)", "mazzetti", "pollo,", "acida", "yogurt)", "(a", "piacere)", "dissalati", "saporito", "bulgur", "grossi", "cerfoglio", "povere", "colmo", "impasto", "surgelate", "galà", "ciliegino", "stellato", "ramo", "miste", "(zucchine,", "peperoni,", "carote,", "cipollotti)", "sbucciate", "cotte", "1-2", "vaso", "ben", "latticello", "tagliati", "tradizionale", "natale", "ripieno", "misti", "(porcini,", "chiodini,", "saraceno", "golden", "1⁄2", "lessa", "menta:", "secco)", "trevigiani", "passa", "puliti", "messicane", "pacco", "coste", "sfoglia:", "ripieno:", "verdure:", "medi", "insalate)", "arborio", "(circa)", "family", "nocciole)", "varie", "pulita", "finferli", "pioppini)", "prezzemolo,", "giallo,", "verde)", "dragoncello", "madera", "sode", "pugno", "di salsicce", "d’olio", "d’erbe:", "caspi", "trevigiano", "dissalate", "tesa", "altre", "tenere", "cuocere)", "altamura", "d’ananas", "toma", "ciliegini", "spagna", "pirottini", "all’origano,", "(spinacini,", "bietoline,", "ortiche,", "cicorietta....)", "che", "fonde", "(fontina,", "simile...)", "trattati", "alta", "qualità", "alcune", "chitarra", "1/4", "veraci", "l’acqua", "della", "shitake", "primavera", "aromatiche:", "classici", "famiglia", "cayenna", "1/3", "d’", "colorata", "snocciolate", "lemongrass", "granny", "smith", "stark", "interi", "legno,", "bambù", "plexiglass", "2-3", "precotta", "pezzetti", "roquefort", "gli", "code", "d’indivia", "grancroccanti", "granspiedì", "(bianco", "nero)", "teryiaki", "patate:", "bianchi", "i", "(pronto", "fatto", "casa)", "mirin", "chiaro", "(quello", "sushi)", "ripiena", "aromatizzato", "americane", "vario", "colore", "neri", "colmi", "minor", "1,2", "lessate", "norcina", "tonnata:", "sodo", "scatoletta", "all’olio", "sott'olio", "pesca", "chia", "(verdi,", "(peso", "sgocciolato)", "thai", "tom", "yum", "(spezie)", "brisè", "(piccoli)", "tazze", "espresso", "omega3", "(freschi", "surgelati)", "grossa", "stecchini", "legno", "tre", "lo", "baccello", "frullate", "(nocciole,", "pinoli,", "uvetta)", "ciascuna", "cm", "pasticcera", "un’arancia", "montata", "l’impasto", "tortano:", "mantovana", "iceberg)", "forni", "trottelle", "miso", "novelli", "vapore", "troppo", "pelati)", "tritata", "olandese", "muffin", "inglesi", "greche", "pasticceria", "stampi", "pasqua", "termometro", "sbattute", "speziata:", "valdostane", "teriyaki:", "peperoni:", "denso", "rasi", "trita", "barattoli", "lattine", "radici", "triturato"];
//estrattore di ricette aia
//itero sui file delle ricette e per ogni file leggo il contenuto e lo faccio parsare per ottenere la ricetta

const startExtraction = async () => {

    //punto di partenza crere un set per tutte le badwords
    var setBadWords = new Set();
    badwordsAIA.forEach(element => {
        if (!setBadWords.has(element)) {
            setBadWords.add(element);
        }
    });

    //parto dalla folder delle ricette aia e ottengo tutti i path dei files
    var aiaSourcesFolder = path.join(__dirname, '..', 'input_sources_aia');
    fs.readdir(aiaSourcesFolder, (err, files) => {
        if (err)
            console.log("errore elaborazione cartella AIA");
        else {
            var filesToExtract = [];
            files.forEach(element => {
                console.log("elaborazione file" + JSON.stringify(element));
                if (element === "primi_recipes.html") filesToExtract.push(path.join(aiaSourcesFolder, "/", element));
            });

            filesToExtract.forEach((element) => {
                let typeRecipe = "Altro";
                if (element.includes("antipasti")) {
                    typeRecipe = "Antipasto";
                }
                else if (element.includes("dolci")) {
                    typeRecipe = "dolce";
                }
                else if (element.includes("piatti_unici")) {
                    typeRecipe = "piatto unico";
                }
                else if (element.includes("primi")) {
                    typeRecipe = "primo";
                }
                else if (element.includes("secondi")) {
                    typeRecipe = "secondo";
                }
                readFileData(element).then((fileData) => {
                    //                    if (element.includes("dolci")) {
                    linkExtractor.extractLinksRecipes(fileData.toString('utf-8')).then((links) => {
                        console.log("links extracted");
                        links.forEach((element, index, elements) => {
                            queue.add(function () {
                                return parseLinkRecipe(element, typeRecipe, setBadWords).catch(err => {
                                    console.log("error in queue");
                                });
                            });
                        });
                    });
                    //                    }
                });
            });
        }
    });
}

function parseLinkRecipe(linkRecipe, typeRecipe, setDabWords) {
    return new Promise((resolve, reject) => {
        AiaRecipeParser.parseAiaRecipe(linkRecipe, sourceAia, baseUrlRecipeAia, urlLogoAia, typeRecipe, setDabWords).then((recipe) => {
            if (!recipe) {
                console.log("error parse RECIPE is NULL")
                reject();
                return
            }
            else if (!validateRecipe(recipe)) {
                console.log("error parse RECIPE has wrong data!");
                reject();
                return
            }
            var aiaFolder = path.join(__dirname, '..', 'recipes', 'aia');
            RecipeRepository.saveRecipeOnFile(aiaFolder, recipe).then(() => {
                RecipeRepository.addNewRecipe(recipe, true).then((data) => {
                    console.log("data saved into database");
                    resolve();
                })
            }).catch(err => {
                console.log("error add recipe in DATABASE: " + err);
                reject();
            });
        }).catch(err => {
            console.log("error PARSE RECIPE: " + err);
            reject();
        });
    })
}

startExtraction().then(() => {
    console.log("end process extraction");
});

//leggere il contenuto del file ed estrarre la stringa
function readFileData(file) {
    return new Promise((resolve, reject) => {
        fs.readFile(file, (err, data) => {
            if (err)
                resolve("false");
            else
                resolve(data);
        });
    });
}

function validateRecipe(recipe) {
    //check se la ricetta ha tutti i campi giusti , soprattutto immagine , tempi , tipo
    return true;
}
