"use_strict";

//uso di open data per estrarre gli ingredienti dal quale poi 

//chiamata http ed estrazione ingredienti dall'html
const _ = require("lodash");
const request = require('requestretry');
const $ = require('cheerio');

const urlData = "https://it.openfoodfacts.org/ingredientes";

//must return an Recipe object
const parseIngredients = async (urlToUse) => {
    const options = {
        url: urlToUse,
        maxAttempts: 3,
        retryDelay: 200,
        retryStrategy: request.RetryStrategies.HTTPOrNetworkError,
    };

    var responseHtmlRecipe = await requestRetryPromise(options);
    //console.log(responseHtmlRecipe.body);
    var ingredients = await parseIngredientsHtml(responseHtmlRecipe.body);
    return ingredients;
}

function requestRetryPromise(optionRequest) {
    return new Promise((resolve, reject) => {
        request(optionRequest, (error, response, body) => {
            resolve({
                "error": error,
                "response": response,
                "body": body
            });
        });
    });
}

const parseIngredientsHtml = async (html) => {
    var ingredients = [];
    var tr = $('tbody > tr', html);

    var ingredientsLinks = tr.find("a");

    for (let i = 0; i < ingredientsLinks.length; i++) {
        try {
            var ingredientParsed = ingredientsLinks[i].children[0].nodeValue;
            ingredients.push(ingredientParsed.split('-').join(' '));
        } catch (err) {
            console.log("errore ingrediente: " + ingredientsLinks[i]);
        }
    }
    return ingredients;
}


parseIngredients(urlData).then((ingredients) => {
    console.log("ingredients extrancted");
    //save ingredients on a file as list data json
    console.log(JSON.stringify(ingredients));
});