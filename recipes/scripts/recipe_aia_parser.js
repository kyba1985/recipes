"use_strict";

const _ = require("lodash");
const request = require('requestretry');
const $ = require('cheerio');
const moment = require("moment");
const util = require('../utils/utilGeneric');
const sharp = require('sharp');
const fs = require("fs-extra");
const hash = require("object-hash");
var path = require('path');
const download = require('image-downloader');
const { Storage } = require('@google-cloud/storage');
const pathKeyGOOGLESTORAGE = "/Users/francescogodino/Desktop/recipes-bot-dev-key-bucket.json"
const projectId = "recipes-bot-dev"
const urlBaseGoogleCloudStorage = "https://storage.googleapis.com/recipesimages/"


const badWordsDefault = ["di", "a", "da", "in", "con", "su", "per", "tra", "fra", "e", "q.b.", "lamelle"];
const quantityTypes = ["ml", "kg", "gr", "g", "l", "dl", "bicchiere", "bicchieri", "cucchiaio", "cucchiaino", "fetta", "fette", "pizzico", "tuorlo", "tuorli", "uova", "stecca",
    "bustina", "confezione", "cucchiai", "bustine", "tazzina", "vasetto", "cucchiaini", "rametti", "foglia", "confezioni", "spicchio", "mazzetto", "mazzo", "rametto", "spicchi", "tazza", "vaschetta", "rotolo", "scaglie", "bottiglia", "lattina", "ciuffo", "bocconcini", "fettine", "fogliolina", "pezzetto", "bicchierino", "bastoncini", "filetti", "foglie", "cespo", "gambo", "busta", "barattolo", "cubetti", "pezzi", "cubetto", "fettina", "litro", "cassetta", "ciuffetto", "scatola", "panetto", "lt", "trancetto", "coscia", "chicchi", "tubetto", "goccio", "grammi", "mazzetti", "pacco", "pugno", "pezzetti", "scatoletta", "tazze", "cm", "barattoli", "lattine"
];

const quantityTypesAlsoIngredients = ["uova", "uovo"];

//must return an Recipe object
const parseAiaRecipe = async (urlToUse, sourceName, urlsourceRecipes, urlLogo, typeRecipe, setDabWords) => {
    const options = {
        url: urlToUse,
        maxAttempts: 3,
        retryDelay: 200,
        retryStrategy: request.RetryStrategies.HTTPOrNetworkError,
    };

    var responseHtmlRecipe = await requestRetryPromise(options);
    console.log(responseHtmlRecipe.response.headers["last-modified"]);
    var timeStampMod = moment(responseHtmlRecipe.response.headers["last-modified"]).valueOf();
    var recipe = await parseRecipeHtml(responseHtmlRecipe.body, timeStampMod, urlToUse, sourceName, urlsourceRecipes, urlLogo, typeRecipe, setDabWords);
    //gestione immagine ricetta
    var imageFolder = path.join(__dirname, '..', 'images', 'recipes', 'aia');
    var imageFolderResize = path.join(__dirname, '..', 'images', 'resize', 'recipes', 'aia');

    const optionsImage = {
        url: recipe.image[0][0],
        dest: imageFolder + "/" + recipe.title + ".jpg"
    }
    await fs.ensureDir(imageFolder)
    await fs.ensureDir(imageFolderResize)
    let downloadOperationSuccessFileName = await downloadIMG(optionsImage)
    if (downloadOperationSuccessFileName) {
        let nameFileResize = (imageFolderResize + "/" + recipe.title + "-" + recipe.source + ".jpg").replace(/\s/g, '-');;
        //resize
        let resizeOperation = await sharp(downloadOperationSuccessFileName)
            .resize(640).jpeg()
            .toFile(nameFileResize);
        //need to upload image on the cloud
        let uploadOperation = await uploadImageOnCloud(nameFileResize, recipe);
        if (resizeOperation && uploadOperation) {
            recipe.image[0][0] = (urlBaseGoogleCloudStorage + recipe.title + "-" + recipe.source + ".jpg").replace(/\s/g, '-');;
            return recipe;
        }
        else
            return null;
    }
    else {
        return null;
    }

}

async function uploadImageOnCloud(pathFileToUpload, recipe) {
    const storage = new Storage({
        projectId: projectId,
        keyFilename: pathKeyGOOGLESTORAGE
    });
    // Uploads a local file to the bucket
    let operation = await storage.bucket('recipesimages').upload(pathFileToUpload, {
        gzip: true,
        metadata: {
            // (If the contents will change, use cacheControl: 'no-cache')
            cacheControl: 'public, max-age=31536000',
        }
    }
    );
    return operation;
}


async function downloadIMG(options) {
    try {
        const { filename, image } = await download.image(options)
        console.log(filename) // => /path/to/dest/image.jpg 
        return filename
    } catch (e) {
        console.error(e)
        return null
    }
}


function requestRetryPromise(optionRequest) {
    return new Promise((resolve, reject) => {
        request(optionRequest, (error, response, body) => {
            resolve({
                "error": error,
                "response": response,
                "body": body
            });
        });
    });
}

const parseRecipeHtml = async (recipeHtml, timeStampMod, urlRecipe, sourceName, urlsourceRecipes, urlLogo, typeRecipe, setDabWords) => { //parse html AIA to retrieve recipe data
    //todo define Recipe in TS and create new Recipe here and get values
    var recipe = {};
    recipe.updateTimeStampEpoch = timeStampMod;

    //titolo
    var metas = $('meta', recipeHtml);
    const metasArray = [];
    for (let i = 0; i < metas.length; i++) {
        metasArray.push($('meta', recipeHtml)[i]);
    }

    let titoloMeta = _.find(metasArray, function (element) {
        return element.attribs.property && element.attribs.property === "og:title"
    });

    console.log(titoloMeta.attribs.content);
    recipe.title = titoloMeta.attribs.content.toLowerCase();

    //difficolta
    var divDifficolta = $('.icon-chef', recipeHtml);
    var strong = divDifficolta.find('strong');
    //console.log(strong.text());
    recipe.difficult = strong.text().toLowerCase();

    //persone
    var divPersone = $('.icon-people', recipeHtml);
    var strong = divPersone.find('strong');
    //console.log(strong.text());
    recipe.people = Number(strong.text().toLowerCase());

    //tempo cottura
    var divCottura = $('.icon-oven', recipeHtml);
    var strong = divCottura.find('strong > div');
    //console.log(strong.text());
    if (strong.text().toLowerCase().includes("ore")) {
        recipe.cookeTime = Number(strong.text().toLowerCase().replace("ore", "").trim()) * 60;
    }
    if (strong.text().toLowerCase().includes("ora")) {
        recipe.cookeTime = Number(strong.text().toLowerCase().replace("ora", "").trim()) * 60;
    }
    else
        recipe.cookeTime = Number(strong.text().toLowerCase().replace("minuti", "").trim());

    //tempo preparazione
    var divPreparazione = $('.icon-pot', recipeHtml);
    var strong = divPreparazione.find('strong > div');
    //console.log(strong.text());
    if (strong.text().toLowerCase().includes("ore")) {
        recipe.totalTime = Number(strong.text().toLowerCase().replace("ore", "").trim()) * 60;
    }
    else
        recipe.preparationTime = Number(strong.text().toLowerCase().replace("minuti", "").trim());

    //tempo totale
    var divTotalTime = $('.icon-timer', recipeHtml);
    var strong = divTotalTime.find('strong > div');
    //console.log(strong.text());
    if (strong.text().toLowerCase().includes("ore")) {
        recipe.totalTime = Number(strong.text().toLowerCase().replace("ore", "").trim()) * 60;
    }
    if (strong.text().toLowerCase().includes("ora")) {
        recipe.totalTime = Number(strong.text().toLowerCase().replace("ora", "").trim()) * 60;
    }
    else
        recipe.totalTime = Number(strong.text().toLowerCase().replace("minuti", "").trim());

    //ingredienti
    var ingredienti = [];
    var ingredientiPuri = [];
    var ingredientiListNode = $('.recipe_ingredients', recipeHtml).find('li > p');
    ingredientiListNode.each(function (i, element) {
        var ingredientObj = {};
        var ingrediente = "";
        element.children.forEach((node) => {
            if (node.nodeValue !== null)
                ingrediente += node.nodeValue; //TODO non basta ci sono anche altri elementi vicino in bold a volte
            else
                ingrediente += node.children[0].nodeValue;
        }); //TODO non basta ci sono anche altri elementi vicino in bold a volte
        if (!ingrediente) //ingrediente con lo strong dentro
        {
            ingrediente = element.children[0].children[0].nodeValue;
        }
        ingredientObj.phrase = ingrediente;

        var words = ingrediente.split(" ");
        //ingrediente con quantità definita , quindi un solo ingrediente da cui devo saltare le bad words
        var hasQuantità = false;
        words.forEach((element) => {
            //if (_.indexOf(badWords, element.toLowerCase()) < 0) //not bad word
            if (!setDabWords.has(element.toLowerCase())) {
                //può essere ingrediente o quantità, verifico prima se quantità
                if (_.indexOf(quantityTypes, element.toLowerCase()) >= 0) {
                    hasQuantità = true;
                    ingredientObj.typeQuantity = element;
                    //check if is also ingrediente
                    if (_.indexOf(quantityTypesAlsoIngredients, element.toLowerCase()) >= 0) {
                        if (_.indexOf(ingredientiPuri, element) < 0)
                            ingredientiPuri.push(element.toLowerCase());
                    }
                }
                else if (!isNaN(element) && hasQuantità === false) { //quantità
                    ingredientObj.quantity = element;
                }
                else if (element.toLowerCase().endsWith("g") | element.toLowerCase().endsWith("gr") | element.toLowerCase().endsWith("kg") | element.toLowerCase().endsWith("l") | element.toLowerCase().endsWith("ml") | element.toLowerCase().endsWith("dl")) //gestione di 80g , 1,5kg . 800gr , 200ml , 5l
                {
                    try {
                        hasQuantità = true;
                        var quantity = element.toLowerCase().match(/\d+/g).map(Number)[0];
                        var type = element.replace(quantity, "");
                        ingredientObj.typeQuantity = type;
                        ingredientObj.quantity = quantity;
                    } catch (err) {
                        console.log("errore quantita: " + element + "err:" + err);
                    }
                }
                else //elemento puro , devo pulire la parola e devo creare anche la gestione plurale e singolare
                {
                    var ingrediente = util.removeBadCharsFromIngredients(element);
                    if (_.indexOf(ingredientiPuri, ingrediente) < 0) {
                        var igredientsPluralSingolar = util.getPluralAndSingolar(element.toLowerCase());
                        ingredientiPuri.push(...igredientsPluralSingolar);
                    }
                }
            }

        })

        ingredientObj.words = words;
        ingredienti.push(ingredientObj);
    })

    console.log("ingredienti puri: " + JSON.stringify(ingredientiPuri));
    recipe.ingredientiPuri = ingredientiPuri;
    recipe.ingredients = ingredienti;
    //console.log(JSON.stringify(ingredienti));

    //note e preparazione
    var recipeIntro = $('.recipes_intro > p', recipeHtml);
    var intro = recipeIntro.text();

    //divisione tra note a preparazione sulla base se trovo una sola stringa preparazione
    var notePreparazione = intro.split("Preparazione");
    if (notePreparazione.length === 2) {
        recipe.intro = notePreparazione[0];
        recipe.instructions = notePreparazione[1];
    }
    else {
        recipe.instructions = intro;
    }

    //la ricetta puo avere diverse sezioni , in tal caso devo fare lavoro aggiuntivo per ottenere le fasi e la preparazione completa
    //se 
    var canvasWhiteInnerDivs = $('.canvas-white > div > div > p', recipeHtml);
    if (canvasWhiteInnerDivs.length > 1) //ricetta con più sezioni
    {
        recipe.instructions = canvasWhiteInnerDivs.text();
    }

    //iframes dentro preparazione
    var canvasWhiteInnerIframe = $('iframe', recipeHtml);
    var videoLinks = [];
    if (canvasWhiteInnerIframe.length > 0) {
        for (let i = 0; i < canvasWhiteInnerIframe.length; i++) {
            videoLinks.push(canvasWhiteInnerIframe[i].attribs.src);
            recipe.hasVideo = true;
        }
    }
    recipe.videoLinks = videoLinks;

    //immagine
    var imageVariations = [];
    var image = [];
    var img = $('.grid-5', recipeHtml).find("source");
    img.each(function (i, element) {
        if (element.attribs.srcset)
            imageVariations.push(element.attribs.srcset);
    })
    if (imageVariations.length > 0)
        image.push(imageVariations);
    else { //ricetta con 2 ricette dentro , la seconda la ignoro adesso ma devo prendere l immagine della prima
        img = $('.grid-6', recipeHtml).find("source");
        img.each(function (i, element) {
            if (element.attribs.srcset)
                imageVariations.push(element.attribs.srcset);
        })
        if (imageVariations.length > 0)
            image.push(imageVariations);

    }
    recipe.image = image;

    //url
    recipe.urlRecipe = urlRecipe;

    //source
    recipe.source = sourceName;

    //urlBase
    recipe.baseUrlRecipes = urlsourceRecipes;

    //logo
    recipe.urlLogoSource = urlLogo;

    //type
    recipe.type = typeRecipe;

    let recipeForHash = recipe;
    let timestampTemp = recipe.updateTimeStampEpoch;
    recipeForHash.updateTimeStampEpoch = 0;

    //hash
    recipe.hash = hash(recipeForHash);
    recipe.updateTimeStampEpoch = timestampTemp; //hash deve evitare di ragionare sul timestamp ma sul contenuto

    console.log(JSON.stringify(recipe));
    return recipe;

}

exports.parseAiaRecipe = parseAiaRecipe;