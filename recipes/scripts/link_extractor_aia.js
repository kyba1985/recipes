"use_strict";

const _ = require("lodash");
const $ = require('cheerio');
var path = require('path');
const basePath = "http://www.aiafood.com";

//partendo da un html stringa ottengo i link alle ricette , al momento non ho 
//capito come estrarle da ajax o link quindi parto da html salvato in file
//in questo script non mi interessa come vengono presi i link ma mi deve essere
//dato in input la stringa html, il tipo di ricetta, la sorgente, il link alla sorgente

//devo dat in output una lista di link delle ricette
async function extractLinksRecipes(htmlRecipes) {
    var linksRecipesExtacted = [];
    var links = $('.grid-3', htmlRecipes).find('a');
    var count = links.length;
    for (let i = 0; i < links.length; i++) {
        linksRecipesExtacted.push(basePath + links[i].attribs.href);
    }
    return linksRecipesExtacted;
}

exports.extractLinksRecipes = extractLinksRecipes;