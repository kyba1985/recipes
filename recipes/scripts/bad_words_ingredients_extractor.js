"use_strict";

var path = require('path');
const fs = require("fs-extra");
const util = require('util');

//accedo al file origine , credo un set con tutti i valori trovati , accedo a quello filtrato , creo un
//altro set , infine dal set estraggo i valori che non sono in quello con gli ingredienti per ottenere una lista di
//bad words da salvare su file

var setAllWords = new Set();
var setIngredients = new Set();
var setBadWords = new Set();
var badwords = [];

const nameFileAll = "ingredients.txt";
const nameFileFiltered = "ingredientsfiltered.txt";

function readFile() {
    var filAllPath = path.join(__dirname, '..', 'ingredients', 'aia', nameFileAll);
    new Promise((resolve, reject) => {
        fs.readFile(filAllPath, function (err, data) {
            let jsonData = JSON.parse(data);
            //per ogni elemento devo aggiungerlo al set
            jsonData.forEach(element => {
                if (!setAllWords.has(element)) {
                    setAllWords.add(element);
                }
            });
            resolve(true)
        });
    }).then(() => {
        var filAllFiltered = path.join(__dirname, '..', 'ingredients', 'aia', nameFileFiltered);
        return new Promise((resolve, reject) => {
            fs.readFile(filAllFiltered, function (err, data) {
                let jsonData = JSON.parse(data);
                //per ogni elemento devo aggiungerlo al set
                jsonData.forEach(element => {
                    if (!setIngredients.has(element)) {
                        setIngredients.add(element);
                    }
                });
                resolve(true);
            });
        });
    }).then(() => {
        //adesso devo iterare su tutti gli elementi del primo set e per ogni elemento
        //che non ha il secondo lo metto in un altro array che poi sara il finale da salvare
        setAllWords.forEach(element => {
            if (!setIngredients.has(element)) {
                if (!setBadWords.has(element)) {
                    setBadWords.add(element);
                }
            }
        });
        //alla fine del set ho le badwords
        setBadWords.forEach(element => {
            badwords.push(element);
        });

        writeFileAsynk(path.join(__dirname, '..', 'ingredients', 'aia'), badwords).then(() => {
            console.log("done");
        });

    });
}

function writeFileAsynk(folderPath, data) {
    var pathFile = path.join(folderPath, "badwords" + ".txt");
    return new Promise((resolve, reject) => {
        fs.ensureDir(folderPath).then(() => {
            fs.writeFile(pathFile, JSON.stringify(data), (err) => {
                if (err)
                    resolve(false);
                else
                    resolve(true);
            });
        });
    });
};

readFile();
