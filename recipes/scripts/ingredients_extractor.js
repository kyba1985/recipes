"use_strict";

var path = require('path');
const fs = require("fs-extra");
const util = require('util');
const readdir = util.promisify(fs.readdir);
//script che usa la cartella dele ricette , al momento aia , e itera su tutti i file per creare
//una hash set con gli ingredienti , poi in un file scrivo un array con i valori dell hash e poi uso
//questo set per creare un array da salvare nel database atlas

const extractIngredients = async () => {
    var set = new Set();
    try {
        //itero su tutti i file dentro aia per adesso
        var aiaSourcesFolder = path.join(__dirname, '..', 'recipes', 'aia');
        var filesToReadPaths = await getFiles(aiaSourcesFolder);
        //per ogni file ottengo aggiungo gli indredienti al set
        for (let i = 0; i < filesToReadPaths.length; i++) {
            var operation = await getIngredientsFromFile(filesToReadPaths[i], set);
        }
        return set;
    } catch (err) {
        console.log("ERROR in extraction ingredients: " + err);
    };
};

const getIngredientsFromFile = async (filePath, set) => {
    //leggere il contenuto del file , parsarlo come json e ottenere gli indredienti e aggiugnerli al set
    var data = await readFileData(filePath);
    var recipeObj = JSON.parse(data);
    var ingredientiPuri = recipeObj.ingredientiPuri;
    //aggiunta ingredienti
    ingredientiPuri.forEach(ingredient => {
        if (!set.has(ingredient)) {
            set.add(ingredient);
        }
    });
    return true;
};

//leggere il contenuto del file ed estrarre la stringa
function readFileData(file) {
    return new Promise((resolve, reject) => {
        fs.readFile(file, (err, data) => {
            if (err)
                resolve("false");
            else
                resolve(data);
        });
    });
}

const getFiles = async (folderPathDir) => {
    var filesPath = [];
    var filesPath = await new Promise((resolve, reject) => {
        readdir(folderPathDir).then(files => {
            files.forEach(element => {
                console.log("elaborazione file" + JSON.stringify(element));
                filesPath.push(path.join(folderPathDir, "/", element));
            });
            resolve(filesPath);
        });
    });
    return filesPath;
};

extractIngredients().then(ingredients => {
    //save ingredients on file and then on db
    var ingredientsArray = [];
    ingredients.forEach(ingredient => {
        ingredientsArray.push(ingredient);
    });
    return ingredientsArray
}).then(arrayIngreditents => {
    //write ingredients on file for now
    return saveIngredeintsOnFile(arrayIngreditents);
}).then(saved => {
    console.log("saved ingredients on file: " + saved);
});

const saveIngredeintsOnFile = async (ingredeints) => {
    try {
        var folderPath = path.join(__dirname, '..', 'ingredients', 'aia');
        result = await writeFileAsynk(folderPath, ingredeints);
        return result;
    } catch (err) {
        console.log(err);
        return false;
    }
}

function writeFileAsynk(folderPath, ingredients) {
    var pathFile = path.join(folderPath, "ingredients" + ".txt");
    return new Promise((resolve, reject) => {
        fs.ensureDir(folderPath).then(() => {
            fs.writeFile(pathFile, JSON.stringify(ingredients), (err) => {
                if (err)
                    resolve(false);
                else
                    resolve(true);
            });
        });
    });
};